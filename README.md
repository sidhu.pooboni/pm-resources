# PM Resources

Resources for aspiring Product managers


# Design
Gestalt principles : [https://www.usertesting.com/blog/gestalt-principles]



# Design Sprint

What’s a Design Sprint and why is it important? : [https://uxplanet.org/whats-a-design-sprint-and-why-is-it-important-f7b826651e09]
The Product Design Sprint : [https://thoughtbot.com/blog/the-product-design-sprint]



# Prototyping
The benefits of high fidelity prototyping :
[https://uxdesign.cc/the-benefits-of-high-fidelity-prototyping-4305a056ffc1#.862k6avhh]

The best prototype is the one you can make :
[https://medium.com/@tjharrop/the-best-prototype-is-the-one-you-can-make-f9992133c8da#.1qyrgzj8g]

Top Ten Benefits of a High-fidelity Prototype :
[http://community.protoshare.com/2010/10/top-ten-benefits-of-a-high-fidelity-prototype/]





# Design Language
https://airbnb.design/building-a-visual-language/



# Case Study
Blue Bottle sprints with GV : [https://sprintstories.com/case-study-blue-bottle-sprints-with-gv-f452789b8ecd#.fx6suvj8b]

Room/roommate finder mobile application : [https://medium.com/@twitalban/ux-ui-case-study-room-roommate-finder-mobile-application-55691938569e]



# Interview

How I Prepared for a Product Manager Interview : 
[https://medium.com/@diemkay/how-i-prepared-for-a-product-manager-interview-26122f2c80ba]

Big List of 90 Product Manager Interview Questions : 
[https://www.impactinterview.com/2017/01/big-list-90-product-manager-interview-questions]

The PM Interview : 
[https://thepminterview.com/]

My Personal Formula for a Winning Resume : 
[https://www.linkedin.com/pulse/20140929001534-24454816-my-personal-formula-for-a-better-resume/]

51 Great Questions to Ask in an Interview : 
[https://www.themuse.com/advice/51-interview-questions-you-should-be-asking]




# Good Blogs

Intro to the CIRCLES Method™ Product Design Framework : 
[https://www.impactinterview.com/2016/06/circles-method-product-design-framework/]

Great Blog to explore :
[https://www.joshdoody.com/]

Tips & Templates for Truly Memorable Product Announcements : 
[https://blog.hubspot.com/blog/tabid/6307/bid/27262/4-tips-for-truly-memorable-product-announcements.aspx?es_p=11394966]

Backlog Bankrupcy :
[https://www.productplan.com/backlog-bankruptcy/]

A Minimal Viable Product needs to actually be viable :
[https://uxdesign.cc/a-minimal-viable-product-needs-to-actually-be-viable-8d121e6f31bd]


# Product Analytics 

Nice read : 
[https://mixpanel.com/blog/2018/09/21/what-is-product-management-analytics/]

Others :
* [https://www.mindtheproduct.com/everything-a-product-manager-needs-to-know-about-analytics/]
* [https://www.sisense.com/blog/how-data-powers-successful-product-management/]
* [https://www.atlassian.com/agile/product-management/product-analytics]
* 


# Tools

Xtensio : A flexible platform where teams create, collaborate on, share and present living documents.

Overflow : User flows done right. Turn your designs into playable user flow diagrams that tell a story.

StoriesOnBoard : User Story Mapping Tool for delivering the right product right on time.

Squid : The Ultimate UX Flow Toolkit. Create Beautiful Flows in just minutes.

Storyboarder : Storyboarder makes it easy to visualize a story as fast you can draw stick figures.

Évolt : Create Business Model Canvas, SWOT analysis, roadmap…